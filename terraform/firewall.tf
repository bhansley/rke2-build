#--- Cluster Load Balancer ---
resource "hcloud_firewall" "cluster_firewall" {
  name               = "fw-cluster"
  rule {
    direction   = "in"
    description = "ICMP in from all sources"
    protocol    = "icmp"
    source_ips  = [
      "0.0.0.0/0",
      "::/0"
    ]
  }

# A more secure option is to only allow ssh/22 in from your work/home/lab IP addr
# To use this, change the 10.10.10.10 address to your public IP and uncomment this block.
#  rule {
#    direction  = "in"
#    description = "ssh in from my location"
#    protocol   = "tcp"
#    port       = "22"
#    source_ips = [
#      "10.10.10.10"
#    ]
#  }

  rule {
    direction   = "in"
    description = "Ingress: http traffic"
    protocol    = "tcp"
    port        = "80"
    source_ips  = [
      "0.0.0.0/0"
    ]
  }


  rule {
    direction   = "in"
    description = "Ingress: https traffic"
    protocol    = "tcp"
    port        = "443"
    source_ips  = [
      "0.0.0.0/0"
    ]
  }


  rule {
    direction   = "in"
    description = "Kubernetes: etcd server client API"
    protocol    = "tcp"
    port        = "2379-2380"
    source_ips  = [
      "0.0.0.0/0"
    ]
  }

  rule {
    direction   = "in"
    description = "Kubernetes: Kubernetes API server"
    protocol    = "tcp"
    port        = "6443"
    source_ips  = [
      "0.0.0.0/0"
    ]
  }

  rule {
    direction   = "in"
    description = "Kubernetes: Kubernetes API server"
    protocol    = "tcp"
    port        = "9345"
    source_ips  = [
      "0.0.0.0/0"
    ]
  }

  rule {
    direction   = "in"
    description = "Kubernetes: Kubelet API (10250), kube-controller-manager (10257), kube-scheduler (10259)"
    protocol    = "tcp"
    port        = "10250-10259"
    source_ips  = [
      "0.0.0.0/0"
    ]
  }
# End of rules

# This is the label selector used to identify resources to apply the firwwall to. In this config, all nodes
# have "type=kub" in their configuration.
  apply_to {
    label_selector   = "type=kub"
  }
}

