### Where variable are declared, potentially described, and provided with default values

terraform {
  required_providers {
    hcloud = {
      source = "hetznercloud/hcloud"
    }
    #hdns = {
    #  source = "alxrem/hdns"
    #}
  }
  required_version = ">= 0.13"
}

variable "hcloud_token" {
  type = string
}
variable "hdns_token" {
  type = string
}

#provider "hdns" {
#  token = var.hdns_token
#}

### SERVER VARS ###
# If you change this location, change the network_location to coordinate.
variable "server_location" {
  default = "fsn1"
}

variable "server_instances" {
  type = map
  default = {
    "servers" = "3"
    "workers" = "3"
  }
}

variable "server_type" {
  type = map
  default = {
    "servers" = "cx21"
    "workers" = "cx21"
  }
}

variable "server_os_image" {
  default = "rocky-9"
}

variable "ssh_key" {
  default = ["hetzner"]
}


### NETWORK VARS ###
# If you change this location, change the server_location to coordinate.
variable "network_location" {
  default = "eu-central"
}


variable "loadbalancer_type" {
  type = map
  default = {
    "cluster" = "lb11"
    "ingress" = "lb11"
  }
}

variable "private_ip_range" {
  default = "10.1.0.0/16"
}

variable "http_protocol" {
  default = "http"
}

variable "http_port" {
  default = "80"
}


