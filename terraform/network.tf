resource "hcloud_network" "cluster_private_network" {
  name     = "cluster_private_network"
  ip_range = var.private_ip_range
}

resource "hcloud_network_subnet" "cluster_private_subnet" {
  network_id   = hcloud_network.cluster_private_network.id
  type         = "cloud"
  network_zone = var.network_location
  ip_range     = var.private_ip_range
}
