# generate inventory file for Ansible
resource "local_file" "hosts_cfg" {
  content = templatefile("${path.module}/templates/hosts.tftpl",
    {
      servers = hcloud_server.rke-servers.*
      workers = hcloud_server.rke-workers.*
    }
  )
  filename = "../ansible/inventory"
}
