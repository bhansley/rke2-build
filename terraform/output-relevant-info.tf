# Share status of a couple of useful items

output "Cluster_LoadBalancer_IPv4" {
  description = "IPv4 of cluster load balancer:"
  value       = "${hcloud_load_balancer.lb-cluster.ipv4}"
}

output "Ingress_LoadBalancer_IPv4" {
  description = "IPv4 of ingress load balancer:"
  value       = "${hcloud_load_balancer.lb-ingress.ipv4}"
}

output "Server0_IP_addr" {
  description = "Server0 IP address:"
  value       = "${hcloud_server.rke-servers[0].ipv4_address}"
}

output "Worker0_IP_addr" {
  description = "Worker0 IP address:"
  value       = "${hcloud_server.rke-workers[0].ipv4_address}"
}

