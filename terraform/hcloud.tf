# Set the variable value in *.tfvars file
# or using the -var="hcloud_token=..." CLI option
# variable "hcloud_token" {}  <-- set in hcloud_token.auto.tfvars

# Configure the Hetzner Cloud Provider
provider "hcloud" {
  token = "${var.hcloud_token}"
}
