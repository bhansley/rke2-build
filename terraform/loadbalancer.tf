#--- Cluster Load Balancer ---
resource "hcloud_load_balancer" "lb-cluster" {
  name               = "lb-cluster"
  load_balancer_type = var.loadbalancer_type["cluster"]
  network_zone       = var.network_location
}

resource "hcloud_load_balancer_network" "clusternetwork" {
  load_balancer_id = hcloud_load_balancer.lb-cluster.id
  network_id       = hcloud_network.cluster_private_network.id
#  ip               = "10.0.1.5"
}

## SERVICE ##
resource "hcloud_load_balancer_service" "lb_service_9345" {
    load_balancer_id = hcloud_load_balancer.lb-cluster.id
    protocol         = "tcp"
    listen_port      = "9345"
    destination_port = "9345"
    health_check {
      protocol      = "tcp"
      port          = "9345"
      timeout 			= 10
      interval			= "15"
      retries	  		= "3"
    }
}

resource "hcloud_load_balancer_service" "lb_service_6443" {
    load_balancer_id = hcloud_load_balancer.lb-cluster.id
    protocol         = "tcp"
    listen_port      = "6443"
    destination_port = "6443"
    health_check {
      protocol      = "tcp"
      port          = "6443"
      timeout 			= "10"
      interval 			= "15"
      retries 			= "3"
    }
}

## TARGETS ##
resource "hcloud_load_balancer_target" "cluster_load_balancer_target" {
  type             = "label_selector"
  load_balancer_id = hcloud_load_balancer.lb-cluster.id
  label_selector   = "node=server"
}



#--- Ingress Load Balancer ----------------------

resource "hcloud_load_balancer" "lb-ingress" {
  name               = "lb-ingress"
  load_balancer_type = var.loadbalancer_type["ingress"]
  network_zone       = var.network_location
  algorithm {
    type = "round_robin"
  }
}

#resource "hcloud_load_balancer_network" "clusternetwork" {
#  load_balancer_id = hcloud_load_balancer.lb-cluster.id
#  network_id       = hcloud_network.cluster_private_network.id
##  ip               = "10.0.1.5"
#}

## SERVICE ##
resource "hcloud_load_balancer_service" "lb_service_80" {
    load_balancer_id = hcloud_load_balancer.lb-ingress.id
    protocol         = "http"
    listen_port      = "80"
    destination_port = "80"
    http {
      sticky_sessions = "true"
    }
    health_check {
      protocol      = "http"
      port          = "80"
      timeout 			= 10
      interval			= "15"
      retries	  		= "3"
      http {
        path         = "/"
        status_codes = ["2??", "3??"]
      } 
    }
}

resource "hcloud_load_balancer_service" "lb_service_443" {
    load_balancer_id = hcloud_load_balancer.lb-ingress.id
    protocol         = "tcp"
    listen_port      = "443"
    destination_port = "443"
    health_check {
      protocol      = "tcp"
      port          = "443"
      timeout 			= "10"
      interval 			= "15"
      retries 			= "3"
    }
}

## TARGETS ##
resource "hcloud_load_balancer_target" "ingress_load_balancer_target" {
  type             = "label_selector"
  load_balancer_id = hcloud_load_balancer.lb-ingress.id
  label_selector   = "node=worker"
}

