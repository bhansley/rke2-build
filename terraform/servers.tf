#### SERVERS ####
resource "hcloud_server" "rke-servers" {
  count       = var.server_instances["servers"]
  name        = "rke2-sv-${count.index}"
  location    = var.server_location
  image       = var.server_os_image
  server_type = var.server_type["servers"]
  ssh_keys    = var.ssh_key
#  placement_group_id = var.placement_group["server"]
  placement_group_id = hcloud_placement_group.server_placement_group.id

  labels = {
    "node" : "server"
    "type" : "kub"
  }
  public_net {
    ipv4_enabled = true
    ipv6_enabled = false
  }
  network {
#    network_id   = var.private_network
    network_id   = hcloud_network.cluster_private_network.id
  }
  user_data = file("cloud-config.yaml")
}

#### WORKERS #####
resource "hcloud_server" "rke-workers" {
  count       = var.server_instances["workers"]
  name        = "rke2-wk-${count.index}"
  location    = var.server_location
  image       = var.server_os_image
  server_type = var.server_type["workers"]
  ssh_keys    = var.ssh_key
#  placement_group_id = var.placement_group["worker"]
  placement_group_id = hcloud_placement_group.worker_placement_group.id

  labels = {
    "node" : "worker"
    "type" : "kub"
  }
  public_net {
    ipv4_enabled = true
    ipv6_enabled = false
  }
  network {
    network_id   = hcloud_network.cluster_private_network.id
#    network_id   = var.private_network
  }
  user_data = file("./cloud-config.yaml")
}

# Create a volume
#resource "hcloud_volume" "storage" {
#  name       = "my-volume"
#  size       = 50
#  server_id  = "${hcloud_server.web.id}"
#  automount  = true
#  format     = "ext4"
#}
