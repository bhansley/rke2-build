resource "hcloud_placement_group" "server_placement_group" {
  name = "server_placement_group"
  type = "spread"
}

resource "hcloud_placement_group" "worker_placement_group" {
  name = "worker_placement_group"
  type = "spread"
}

