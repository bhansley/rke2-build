# rke2-build


## Description
This project started with a few goals:
- Deploy a usable Kubernetes cluster with as little manual work as possible.
- Support HA and non-HA cluster configurations
- Support RWX persistent volumes
- Run for < $50/month.

The project has achieved all but the RWX goal so far, and that one is on the roadmap.

This project uses [RKE2](https://docs.rke2.io), or Rancher Kubernetes Engine for Government, as its Kubernetes version. RKE2 is a Rancher mix of RKE1 and k3s which provided a couple of important features:
- It's like k3s in that it's a simpler, fully conformant deployment.
- Its etcd allows for HA clusters like full Kubernetes, and without the external syncing of k3s.
- RKE2 was built for government use so it has hardened defaults out of the box.

It uses [Terraform](https://www.terraform.io) to create the hosting environment on [Hetzner](https://hetzner.cloud/?ref=biBbHkWgAaWh). Usable shared CPU VMs can be had for ~$8USD/mo, making the final goal doable. 

[Ansible](https://www.ansible.com) is used to deploy RKE2 to the environment created by Terraform. 

## Architecture
![](/Architecture%20Diagram.png)

## Installation

# Requirements
- A [Hetzner hosting account](https://hetzner.cloud/?ref=biBbHkWgAaWh). This should adaptable to other hosting providers, but it's built and tested on Hetzner. 
- Terraform installed locally
- Ansible installed locally
- A registered hostname (eg. foo.com) if you're deploying an HA control plane in your cluster.

# Templates
To discourage/prevent sensitive information like cluster passwords and tokens from getting committed to version control, files that will hold that information are provided as **.tpl** template files. Generally, each template file will be copied to a new file without the .tpl, and the contents of the new file will be edited. The new files that will hold that sensitive information are included in **.gitignore** and should be ignoreed by git.

# Steps
1. Clone this repo to your local machine.
1. Create a new ssh key to use for this project, or identify an existing one to use.
1. In the (Hetzner cloud console)[https://console.hetzner.cloud]:
    1. Create a new project by clicking the **+ New Project** button.
    1. Under the **Security** tab at the left, pick **SSH Keys** at the top and upload the public key from the pair from step 2. The Terraform scripts expect this to be named **hetzner**.
    1. Also on the **Security** tab, pick **API Tokens** at the top. Create a new one, and save it somewhere securely, as you won't be able to access the token a second time. The name you use isn't critical.
1. In the **terraform** directory copy **hcloud_token.auto.tfvars.tpl** to **hcloud_token.auto.tfvars. Edit that file**  and replace **##HCLOUD_TOKEN##** with the API Token you created in step 3.3. Be sure to keep the quotes in place.
1. In the **ansible** directory, copy **node-token-short.tpl** to **node-token-short**. In the new file, replace the entire contents of the file, including the << and >> with a secure password that'll be used to create the RKE2 cluster.
1. `cd terraform`
1. `terraform init` It should complete without errors.
1. `terraform plan` to see what terraform will build in the next step. See the project Terraform README to learn more about the individual components.
1. `terraform apply` to have terraform build the necessary infrastructure on Hetzner. Once it completes, you can go browse the new infrastructure components on the Hetzner Cloud Console.
1. Make note of the `Cluster_LoadBalancer_IPv4 = "1.2.3.4"` Register/update an A record for this IP. I use a subdomain off an existing domain name I already own.
1. `cd ../ansible`
1. Copy **cluster-url.tpl** to **cluster-url** and replace the entire contents of the newly copied file with either:
    1. The IP of a single server node (for non-HA setups) eg. `1.2.3.4``
    1. The a unique cluster load balancer URL (for LB setups). This can be a subdomain off an existing domain registration you have. DNS updates to follow. .eg `foo.com` or `kub.foo.com`
1. `ansible -i hosts all -m ping --private-key={path to your private key}` This confirms connection to all the hosts using the ssh keypair, and it adds entries for each host to your **~/.ssh/known_hosts** file.
1. `cp node-token-short.tpl node-token-short` and then replace the entire contents of the **node-token-short** file with a secure token/password that will be used to build the cluster. Be cautious of special characters in the token.
1. `ansible-playbook -i hosts users.yaml --private-key={path to your private key}`
1. `ansible-playbook -i hosts install-server0.yaml --private-key={path to your private key}`  *
1. `ansible-playbook -i hosts install-servers.yaml --private-key={path to your private key}`  *
1. `ansible-playbook -i hosts install-workers.yaml --private-key={path to your private key}` *
1. While running the server install scripts, they will copy a kubeconfig.yaml file to your ansible directory. Check this file for validity, especially regarding the hostname of the cluster. 
1. You can run `kubectl get pods -A` or other kubeernetes commands to interact with your cluster - eg. deploying workloads, etc. Your cluster is ready to use!
1. If this was a practice deployment for you, cd back to your terraform directory and run `terraform destroy` - and confirm - to tear down all the infrastructure you created earlier. This will also stop the billing. Confirm that any chargable infrastructure is gone in the Hetzner Cloud Console.

* If any of these build steps fail, tear down the environment with a `terraform destroy`, double check your configurations, esp steps 10 and 12, as those seem to be the most prone to error, and restart with step 9. 

Next Steps:
- Go into `terraform/variables.tf` to change the counts and server types for servers and worker nodes, to try larger, smaller, or differently shaped cluster configurations.
- For longer-term, more secure operation of the cluster, edit `terraform/firewall.tf` and in the commented-out ssh section, add your IP (or range), uncommend the section, and apply. This will only allow ssh from your location.

## Support
If you run into problems or have questions, please open an issue in Gitlab.

## Roadmap
- Use Terraform hooks to Hetzner's DNS service to automate that part of the build. It's not hard, but it does add another fussy prerequisite.
- Add Longhorn for persistent storage. That'll satisfy the RWX storage goal.
- Add Gitlab's Agent for Kubernetes to deploy workloads to the cluster automatically after the cluster is built.
- Alternate versions of this project that deploy other Kubernetes flavors like Rancher and OKD.


## Notes
- Some Hetzner links are referral links. Free Hetzner credit for you and me if you sign up for an account using that link. 

## Authors and acknowledgment
Thanks to the following site, which were instrumental in putting this project together:
- AlphaBravo's engineering blog post on RKE2 and Hetzner. [Link](https://blog.alphabravo.io/posts/2021/single-node-rke2-pt1/)

## License
This work is licensed under the MIT license.

